<?php

return [
  'push_url' => env('PUSH_URL', 'https://fcm.googleapis.com/fcm/send'),
  'server_key' => env('FCM_SERVER_KEY')
];
