<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('description'); //A short description of the wing's activities or purpose.
            $table->string('email'); //The email of the wing --optional
            $table->string('slogan');
            $table->string('slug');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        DB::unprepared(
          'CREATE TRIGGER wings_update_trigger AFTER UPDATE ON `wings` FOR EACH ROW
              BEGIN
                 UPDATE `wings` SET `updated_at` = CURRENT_TIMESTAMP;
              END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wings');
        DB::unprepared('DROP TRIGGER IF EXISTS wings_update_trigger');
    }
}
