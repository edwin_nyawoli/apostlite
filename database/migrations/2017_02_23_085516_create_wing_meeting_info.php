<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWingMeetingInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wing_meeting_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wing_id')->unsigned();
            $table->string('location');
            $table->enum('day', ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']);
            $table->time('time');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->index('wing_id');
            $table->foreign('wing_id')->references('id')->on('wings')->onDelete('cascade');
        });

        DB::unprepared(
          'CREATE TRIGGER wing_meeting_infos_update_trigger AFTER UPDATE ON `wing_meeting_infos` FOR EACH ROW
              BEGIN
                 UPDATE `wing_meeting_infos` SET `updated_at` = CURRENT_TIMESTAMP;
              END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wing_meeting_infos');
        DB::unprepared('DROP TRIGGER IF EXISTS wing_meeting_infos_update_trigger');
    }
}
