<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWingExecutives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wing_executives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wing_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('position');
            $table->string('year_group');
            $table->string('profile_photo')->nullable();
            $table->string('bio')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->string('slug');
            $table->index('wing_id');
            $table->foreign('wing_id')->references('id')->on('wings')->onDelete('cascade');
        });

        DB::unprepared(
          'CREATE TRIGGER wing_executives_update_trigger AFTER UPDATE ON `wing_executives` FOR EACH ROW
              BEGIN
                 UPDATE `wing_executives` SET `updated_at` = CURRENT_TIMESTAMP;
              END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wing_executives');
        DB::unprepared('DROP TRIGGER IF EXISTS wing_executives_update_trigger');
    }
}
