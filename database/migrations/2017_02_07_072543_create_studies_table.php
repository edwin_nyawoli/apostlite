<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('study_number')->unique();
            $table->string('title');
            $table->string('scriptures'); //scriptures supporting the study
            $table->string('content');
            $table->string('questions'); //All the questions are concatenated into a single string b4 being
            $table->smallInteger('year');
            $table->string('slug');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        DB::unprepared(
          'CREATE TRIGGER studies_update_trigger AFTER UPDATE ON `studies` FOR EACH ROW
              BEGIN
                 UPDATE `studies` SET `updated_at` = CURRENT_TIMESTAMP;
              END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studies');
        DB::unprepared('DROP TRIGGER IF EXISTS studies_update_trigger');
    }
}
