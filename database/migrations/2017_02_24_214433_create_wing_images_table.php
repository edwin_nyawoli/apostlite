<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWingImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wing_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wing_id')->unsigned();
            $table->string('year_group');
            $table->string('group_image')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->string('slug');
            $table->index('wing_id');
            $table->foreign('wing_id')->references('id')->on('wings')->onDelete('cascade');
        });

        DB::unprepared(
          'CREATE TRIGGER wing_images_update_trigger AFTER UPDATE ON `wing_images` FOR EACH ROW
              BEGIN
                 UPDATE `wing_images` SET `updated_at` = CURRENT_TIMESTAMP;
              END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wing_images');
        DB::unprepared('DROP TRIGGER IF EXISTS wing_images_update_trigger');
    }
}
