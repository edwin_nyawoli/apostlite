<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSermonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sermons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('minister');
            $table->string('scriptures'); //The scriptures used in the sermon separated by ,
            $table->string('message');
            $table->string('image')->nullable();
            $table->string('slug');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        DB::unprepared(
          'CREATE TRIGGER sermon_update_trigger AFTER UPDATE ON `sermons` FOR EACH ROW
              BEGIN
                 UPDATE `sermons` SET `updated_at` = CURRENT_TIMESTAMP;
              END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sermons');
        DB::unprepared('DROP TRIGGER IF EXISTS sermon_update_trigger');
    }
}
