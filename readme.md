# Apostlite

Apostlite is a system to help the leaders of NUPS-G KNUST Local to communicate information directly to
members of the church. It allows leaders to:
  - Send news and notifications directly to the devices of members or anyone using
  the companion android app.
  - Send sermons to members to allow them to still meditate on the word even
  after leaving church members. This also helps members who could not make it to church
  to still get the word that was preached.
  - Provide information on Bible Studies, including those of previous years.
  - Provide information about the various wings in the church along with their executives(both current and past).
  - Provide information on events or program that would occur during the semester
  or even after.

 **Apostlite was built using the [Laravel](https://laravel.com) framework.**

## Setting it up
#### Composer

  You'll need composer to get the dependencies required. If you do not already have Composer installed, you can get it [here](https://getcomposer.org/).

Apostlite was built using the [Laravel](https://laravel.com) framework


#### After cloning the repository,
  - Create an .env in the project root, or rename the .env.example to .env
  - Change the environment variables in the .env file to your own.
    (ie. the database name, database username and password, etc)
  - Run composer update in the project root to make sure you have all the dependencies.
  - Run the command php artisan key:generate to generate a new key for the app.
  - Run the command php artisan config:clear to clear any previous configurations.

#### Using the default database.
  - To use the default database, create an 'apostlitedb.sqlite' file in the database directory and comment out DB_DATABASE in your .env file.
  - Run the migrations to create the actual tables in the database.
  
  ![Built With Love](http://forthebadge.com/images/featured/featured-built-with-love.svg)

Copyright &copy; 2017 NUPS-G KNUST Local.
