<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


// News routes
Route::resource('news', 'NewsController');


// Sermon routes
Route::resource('sermons', 'SermonController');


// Study routes
Route::resource('studies', 'StudyController');


// Wing routes
Route::get('/wings', 'WingController@index');
Route::get('/wings/executives', 'WingController@indexExecutives');
Route::get('/wings/gallery', 'WingController@indexGallery');

Route::post('/wings', 'WingController@store');
Route::post('/wings/executives', 'WingController@storeExecutive');
Route::post('/wings/gallery', 'WingController@storeGalleryImage');

Route::get('/wings/create', 'WingController@create');
Route::get('/wings/executives/create', 'WingController@createExecutive');
Route::get('/wings/gallery/create', 'WingController@createGalleryImage');

Route::get('/wings/executives/{executive}', 'WingController@showExecutive');
Route::get('/wings/gallery/{image}', 'WingController@showGalleryImage');
Route::get('/wings/{wing}', 'WingController@show');

Route::put('/wings/executives/{executive}', 'WingController@updateExecutive');
Route::patch('/wings/executives/{executive}', 'WingController@updateExecutive');
Route::put('/wings/gallery/{image}', 'WingController@updateGalleryImage');
Route::patch('/wings/gallery/{image}', 'WingController@updateGalleryImage');
Route::put('/wings/{wing}', 'WingController@update');
Route::patch('/wings/{wing}', 'WingController@update');

Route::delete('/wings/executives/{executive}', 'WingController@destroyExecutive');
Route::delete('/wings/gallery/{image}', 'WingController@destroyGalleryImage');
Route::delete('/wings/{wing}', 'WingController@destroy');

Route::get('/wings/executives/{executive}/edit', 'WingController@editExecutive');
Route::get('/wings/gallery/{image}/edit', 'WingController@editGalleryImage');
Route::get('/wings/{wing}/edit', 'WingController@edit');


// Program routes
Route::resource('programs', 'ProgramController');
