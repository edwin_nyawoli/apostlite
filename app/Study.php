<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Study extends Model
{
    use Sluggable;
    protected $fillable = [
      'study_number',
      'title',
      'scriptures',
      'content',
      'questions',
      'year'
    ];

    public function scopeFindSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function scopeYear($query, $year)
    {
      if(!is_null($year))
        return $query->where('year', '=', $year);

      return $query;
    }

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
           'slug' => [
               'source' => 'title'
           ]
        ];
    }
}
