<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Sermon extends Model
{
    use Sluggable;
    protected $fillable = [
      'title',
      'minister',
      'scriptures',
      'message',
      'image'
    ];

    public function scopeFindSlug($query, $slug)
    {
      return $query->where('slug', $slug);
    }

    public function scopePublishedAfter($query, $date)
    {
        if(!is_null($date))
          return $query->whereRaw("DATE(created_at) > DATE($date)");

        return $query;
    }

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
           'slug' => [
               'source' => 'title'
           ]
        ];
    }
}
