<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class News extends Model
{
    use Sluggable;
    protected $fillable = [
      'title',
      'content'
    ];

    public function scopeFindSlug($query, $slug)
    {
      return $query->where('slug', $slug);
    }

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
           'slug' => [
               'source' => 'title'
           ]
        ];
    }
}
