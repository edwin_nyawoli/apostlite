<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Program extends Model
{
    use Sluggable;
    protected $fillable = [
      'name',
      'venue',
      'banner',
      'start_date',
      'end_date'
    ];

    public function scopeFindSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function scopePublishedAfter($query, $date)
    {
        if(!is_null($date))
          return $query->whereRaw("DATE(created_at) > DATE($date)");

        return $query;
    }

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
           'slug' => [
               'source' => 'name'
           ]
        ];
    }
}
