<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wing;
use App\WingExecutive;
use App\WingImage;
use App\WingMeetingInfo;
use App\Traits\ApiRequestCheck;

class WingController extends Controller
{
    use ApiRequestCheck;
    public function __construct() {
      $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $wings = Wing::all();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $wings;

        return view('list.wings', compact('wings'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexExecutives(Request $request)
    {
        $wingExecutives = WingExecutive::all();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $wingExecutives;

        return view('list.wing-executives', compact('wingExecutives'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexGallery(Request $request)
    {
        $wingImages = WingImage::all();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $wingImages;

        return view('list.wing-gallery', compact('wingImages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create.wing');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createExecutive()
    {
        $wingNames = Wing::select('name')->get();
        $currentYear = \Carbon\Carbon::now()->year;
        for ($i = 0; $i < 10; $i++) {
            $yearGroups[] = ($currentYear - $i) . '-' . ($currentYear - $i + 1);
        }
        return view('create.wing-executive', compact('wingNames', 'yearGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createGalleryImage()
    {
        $wingNames = Wing::select('name')->get();
        return view('create.wing-image', compact('wingNames'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wing = Wing::create($request->all());
        $locations = $request->get('location');
        $days = $request->get('day');
        $times = $request->get('time');
        $meetingInfoSize = count($locations);

        for ($index = 0; $index < $meetingInfoSize; $index++) {
            $meetingInfo = new WingMeetingInfo();
            $meetingInfo->location = $locations[$index];
            $meetingInfo->day = $days[$index];
            $meetingInfo->time = $times[$index];
            $meetingInfo->wing_id = $wing->id;
            $meetingInfo->save();
        }

        return redirect('/wings');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExecutive(Request $request)
    {
        $wing = Wing::select('id')->where('name', '=', $request->get('wing_name'))->first();
        $executive = new WingExecutive($request->all());
        if($wing->id != -1) {
          $executive->wing_id = $wing->id;
          $executive->save();
        }

        return redirect('/wings/executives');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeGalleryImage(Request $request)
    {
        $wing = Wing::select('id')->where('name', '=', $request->get('wing_name'))->first();
        $wingImage = new WingImage($request->all());
        if($wing->id != -1) {
          $wingImage->wing_id = $wing->id;
          $wingImage->save();
        }
        return redirect('/wings/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return redirect('/wings');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $wing = Wing::findSlug($slug)->first();
        $wingMeetingInfos = $wing->meetingInfos()->get();
        return view('edit.wing', compact('wing', 'wingMeetingInfos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function editExecutive($slug)
    {
        $wings = Wing::select('id', 'name')->get();
        $wingExecutive = WingExecutive::findSlug($slug)->first();
        $currentYear = \Carbon\Carbon::now()->year;
        for ($i = 0; $i < 10; $i++) {
            $yearGroups[] = ($currentYear - $i) . '-' . ($currentYear - $i + 1);
        }
        return view('edit.wing-executive', compact('wingExecutive', 'wings', 'yearGroups'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function editGalleryImage($slug)
    {
        $wingImage = WingImage::findSlug($slug)->first();
        return view('edit.wing-image', compact('wingImage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('/wings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wing::destroy($id);
        return redirect('/wings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyExecutive($id)
    {
        WingExecutive::destroy($id);
        return redirect('/wings/executives');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyGalleryImage($id)
    {
        WingImage::destroy($id);
        return redirect('/wings/gallery');
    }
}
