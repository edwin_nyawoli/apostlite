<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Traits\ApiRequestCheck;
use App\FirebaseCloudMessage;
use Config;
use Exception;

class ProgramController extends Controller
{
    use ApiRequestCheck;
    public function __construct() {
      $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programs = Program::latest('created_at')->publishedAfter($request->query('publishedAfter'))->get();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $programs;

        return view('list.programs', compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('create.program');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $program = Program::create($request->all());
        $serverKey = Config::get('notifications.server_key');
        $url = Config::get('notifications.push_url');

        try {
          $fcMessage = new FirebaseCloudMessage($url, $serverKey);
          $fcMessage->to('/topics/programs');
          $fcMessage->notification('News Program', $program->name);
          //TODO Find a way to know if something is wrong and react accordingly.
          $fcmResponse =  $fcMessage->send();
          return redirect('/programs');
        } catch(Exception $e) {
          return sprintf('Firebase message sending failed with error #%d: %s', $e->getCode(), $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $program = Program::findSlug($slug)->first();
        $hash = base64_encode($slug);
        return view('edit.program', compact('program', 'hash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $path = $request->path();
        $slug = base64_decode(substr($path, strpos($path, "/") + 1));
        $program = Program::findSlug($slug)->first();
        $program->update($request->all());
        return redirect('/programs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Program::destroy($id);
        return redirect('/programs');
    }
}
