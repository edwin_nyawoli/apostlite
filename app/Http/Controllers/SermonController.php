<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sermon;
use App\FirebaseCloudMessage;
use App\Traits\ApiRequestCheck;
use Config;
use Exception;

class SermonController extends Controller
{
    use ApiRequestCheck;
    public function __construct() {
      $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sermons = Sermon::latest('created_at')->publishedAfter($request->query('publishedAfter'))->get();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $sermons;

        return view('list.sermons', compact('sermons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create.sermon');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sermon = Sermon::create($request->all());

        $serverKey = Config::get('notifications.server_key');
        $url = Config::get('notifications.push_url');

        try {
          $fcMessage = new FirebaseCloudMessage($url, $serverKey);
          $fcMessage->to('/topics/sermons');
          $fcMessage->notification('New Sermon', $sermon->title . ' by ' . $sermon->minister);
          //TODO Find a way to know if something is wrong and react accordingly.
          $fcmResponse =  $fcMessage->send();
          return redirect('/sermons');
        } catch(Exception $e) {
          return sprintf('Firebase message sending failed with error #%d: %s', $e->getCode(), $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $sermon = Sermon::findSlug($slug)->first();
        $hash = base64_encode($slug);
        return view('edit.sermon', compact('sermon', 'hash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $path = $request->path();
        $slug = base64_decode(substr($path, strpos($path, "/") + 1));
        $sermon = Sermon::findSlug($slug)->first();
        $sermon->update($request->all());
        return redirect('/sermons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sermon::destroy($id);
        return redirect('/sermons');
    }
}
