<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Study;
use App\Traits\ApiRequestCheck;
use Carbon\Carbon;
use App\FirebaseCloudMessage;
use Config;
use Exception;

class StudyController extends Controller
{
    use ApiRequestCheck;
    public function __construct() {
      $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $studies = Study::orderBy('study_number')->year($request->query('year'))->get();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $studies;

        return view('list.studies', compact('studies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('create.study');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['year'] = Carbon::now()->year;

        $study = Study::create($input);
        $serverKey = Config::get('notifications.server_key');
        $url = Config::get('notifications.push_url');

        try {
          $fcMessage = new FirebaseCloudMessage($url, $serverKey);
          $fcMessage->to('/topics/bible-studies');
          $fcMessage->notification('Bible Study', $study->name);
          //TODO Find a way to know if something is wrong and react accordingly.
          $fcmResponse =  $fcMessage->send();
          return redirect('/studies');
        } catch(Exception $e) {
          return sprintf('Firebase message sending failed with error #%d: %s', $e->getCode(), $e->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $study = Study::findSlug($slug)->first();
        return view('edit.study', compact('study'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $path = $request->path();
        $slug = base64_decode(substr($path, strpos($path, "/") + 1));
        $study = Study::findSlug($slug)->first();
        $study->update($request->all());
        return redirect('/studies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Study::destroy($id);
        return redirect('/studies');
    }
}
