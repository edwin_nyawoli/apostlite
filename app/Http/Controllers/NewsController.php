<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\FirebaseCloudMessage;
use Config;
use Exception;
use App\Traits\ApiRequestCheck;

class NewsController extends Controller
{
    use ApiRequestCheck;
    public function __construct() {
      $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::all();
        if($request->wantsJson() || $this->isApiRequest($request))
          return $news;

        return view('list.news', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('create.news');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = News::create($request->all());
        $serverKey = Config::get('notifications.server_key');
        $url = Config::get('notifications.push_url');

        try {
          $fcMessage = new FirebaseCloudMessage($url, $serverKey);
          $fcMessage->to('/topics/news');
          $fcMessage->notification('News Update', $news->title);
          //TODO Find a way to know if something is wrong and react accordingly.
          $fcmResponse =  $fcMessage->send();
          return redirect('/news');
        } catch(Exception $e) {
          return sprintf('Firebase message sending failed with error #%d: %s', $e->getCode(), $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return redirect('/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
