<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WingMeetingInfo extends Model
{
    protected $fillable = [
      'location',
      'day',
      'time'
    ];

    public function wing()
    {
        return $this->belongsTo('App\Wing');
    }
}
