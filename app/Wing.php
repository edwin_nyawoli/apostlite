<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Wing extends Model
{
    use Sluggable;
    protected $fillable = [
      'name',
      'description',
      'slogan',
      'email',
      'group_image'
    ];



    public function scopeFindSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function meetingInfos()
    {
      return $this->hasMany('App\WingMeetingInfo');
    }

    public function executives()
    {
      return $this->hasMany('App\WingExecutive');
    }

    public function images()
    {
      return $this->hasMany('App\WingImage');
    }

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
           'slug' => [
               'source' => 'name'
           ]
        ];
    }
}
