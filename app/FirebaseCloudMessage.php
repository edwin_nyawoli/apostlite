<?php
namespace App;
use Exception;

class FirebaseCloudMessage {
  private $_body;
  private $_url;
  private $_headers;

  public function __construct($url, $serverKey) {
    $this->_url = $url;
    $this->_headers = array();
    $this->_headers[] = 'Content-Type: application/json';
    $this->_headers[] = 'Authorization:' . ' key=' . $serverKey;
    $this->_body = array();
  }

  public function to($to) {
    $this->_body['to'] = $to;
  }

  public function condition($condition) {
    $this->_body['condition'] = $condition;
  }

  public function data($data) {
    $this->_body['data'] = $data;
  }

  public function notification($title = false, $body = false, $icon = false) {
    if($title != false)
      $this->_body['notification']['title'] = $title;

    if($body != false)
      $this->_body['notification']['body'] = $body;

      if($icon != false)
        $this->_body['notification']['icon'] = $icon;
  }

  public function send() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
       CURLOPT_URL => $this->_url,
       CURLOPT_HEADER => true,
       CURLOPT_SSL_VERIFYPEER => false,           // TODO Remove debug cert disabling below.
       CURLOPT_VERBOSE => true,
       CURLOPT_HTTPHEADER => $this->_headers,
       CURLOPT_POST => true,
       CURLOPT_POSTFIELDS => json_encode($this->_body),
       CURLOPT_RETURNTRANSFER => true
    ));

    $curl_result = curl_exec($curl);


    if($curl_result == FALSE)
      throw new Exception(curl_error($curl), curl_errno($curl));

    curl_close($curl);
    return json_encode($curl_result);
  }

  public function error() {

  }
}
