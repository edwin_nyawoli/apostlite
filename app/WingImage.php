<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class WingImage extends Model
{
    use Sluggable;
    protected $fillable = [
      'year_group',
      'group_image'
    ];

    public function scopeFindSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function wing()
    {
        return $this->belongsTo('App\Wing');
    }

    public function scopeYearGroup($query, $yearGroup)
    {
      if(!is_null($yearGroup))
        return $query->where('year_group', '=', $yearGroup);

      return $query;
    }

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
           'slug' => [
               'source' => 'group_image'
           ]
        ];
    }
}
