<?php
namespace App\Traits;
use Illuminate\Http\Request;

trait ApiRequestCheck {
    public function isApiRequest(Request $request) {
      return strcmp($request->segments()[0], 'api') == 0;
    }
}
