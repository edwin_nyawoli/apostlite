@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>Bible Studies</h4>
    </div>
    <div class="panel-body">
      @if(count($studies) == 0)
        No bible study has been added yet. Click <span style="font-weight: bold;">Add New</span> to add a bible study.
        <a class="btn btn-success" href="/studies/create">Add New</a>
      @else
        <a class="btn btn-success" href="/studies/create">Add New</a>
        <h4>{{ count($studies) }} Bible Stud
          @if(count($studies) > 1)
            ies
          @else
            y
          @endif
        .</h4>

        <table class="table table-hover">
          <tr>
            <th>Title</th>
            <th>Actions</th>
          </tr>

          @foreach($studies as $study)
            <tr>
              <td>{{ $study->title }}</td>
              <td>
                <!--  TODO Replace id with slug -->
                <a class="btn btn-info" href="/studies/{{ $study->slug }}/edit">
                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
                <button class="btn btn-danger btn-del" type="button" name="button" data-toggle="modal" data-target="#confirmDelete" data-index="{{ $study->id }}">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
              </td>
            </tr>
          @endforeach
        </table>
      @endif
    </div>
  </div>

  <!-- Confirm delete modal -->
  <div class="modal fade" id="confirmDelete" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <span class="modal-title">Delete Study</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this bible study?</p>
          <form id="deleteItem" action="{{ url('/studies') }}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" form="deleteItem" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    var form = $('#deleteItem');
    var delBtns = $('.btn-del').click(function(){
      form.attr('action', '/studies/' + $(this).data('index'));
    });
  </script>
@endsection
