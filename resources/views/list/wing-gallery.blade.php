@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>Gallery</h4>
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/wings') }}">Wings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/wings/executives') }}">Executives</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{{ url('/wings/gallery') }}">Gallery</a>
        </li>
      </ul>
    </div>
    <div class="panel-body">
      @if(count($wingImages) == 0)
        No images have been added yet. Click <span style="font-weight: bold;">Add New</span> to add an image for a wing.
        <a class="btn btn-success" href="{{ url('/wings/gallery/create') }}">Add New</a>
      @else
        <a class="btn btn-success" href="{{ url('/wings/gallery/create') }}">Add New</a>
        <h4>{{ count($wingImages) }} Image(s).</h4>
        @foreach($wingImages as $image)
          <div class="row">
            <div class="col-xs-6 col-md-3">
              <a href="#" class="thumbnail">
                <img src="{{ $image->group_image }}" alt="">
              </a>
            </div>
          </div>
        @endforeach

      @endif
    </div>
  </div>

  <!-- Confirm delete modal -->
  <div class="modal fade" id="confirmDelete">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <span class="modal-title">Delete Wing Image</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this image?</p>
          <form id="deleteItem" action="{{ url('/wings') }}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" form="deleteItem" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    var form = $('#deleteItem');
    var delBtns = $('.btn-del').click(function(){
      form.attr('action', '/wings/gallery/' + $(this).data('index'));
    });
  </script>
@endsection
