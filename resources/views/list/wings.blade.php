@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>Wings</h4>
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" href="{{ url('/wings') }}">Wings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/wings/executives') }}">Executives</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/wings/gallery') }}">Gallery</a>
        </li>
      </ul>
    </div>
    <div class="panel-body">
      @if(count($wings) == 0)
        No wing has been added yet. A wing must first be added before it's executives or gallery images can be added.
        Click <span style="font-weight: bold;">Add New</span> to add a wing.
        <a class="btn btn-success" href="{{ url('/wings/create') }}">Add New</a>
      @else
        <a class="btn btn-success" href="{{ url('/wings/create') }}">Add New</a>
        <h4>{{ count($wings) }} Wing(s).</h4>
        <table class="table table-hover">
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
          </tr>
          @foreach($wings as $wing)
            <tr>
              <td>{{ $wing->name }}</td>
              <td>{{ $wing->email }}</td>
              <td>
                <a class="btn btn-info" href="/wings/{{ $wing->slug }}/edit">
                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
                <button class="btn btn-danger btn-del" type="button" name="button" data-toggle="modal" data-target="#confirmDelete" data-index="{{ $wing->id }}">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
              </td>
            </tr>
          @endforeach
        </table>
      @endif
    </div>
  </div>

  <!-- Confirm delete modal -->
  <div class="modal fade" id="confirmDelete">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <span class="modal-title">Delete Wing</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this wing?</p>
          <form id="deleteItem" action="{{ url('/wings') }}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" form="deleteItem" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    var form = $('#deleteItem');
    var delBtns = $('.btn-del').click(function(){
      form.attr('action', '/wings/' + $(this).data('index'));
    });
  </script>
@endsection
