@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>News</h4>
    </div>
    <div class="panel-body">

      @if(count($news) == 0)
        No news post has been added yet.
        @if(Auth::user())
          Click <span style="font-weight: bold;">New</span> to create a news post.
          <!-- <button class="btn btn-success" type="button" data-toggle="modal" data-target="#addNewModal">New</button> -->
          <a class="btn btn-success" href="/news/create">New</a>
        @else
          Please check back soon.
        @endif
      @else
        <a class="btn btn-success" href="/news/create">New</a>
        <h4>{{ count($news) }} News post(s).</h4>
        <span>Posts here have been sent to users.</span>
        <table class="table table-hover">
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Date Published</th>
            <th>Actions</th>
          </tr>
          @foreach($news as $post)
            <tr>
              <td>{{ $post->id }}</td>
              <td>{{ $post->title }}</td>
              <td>{{ $post->created_at }}</td>
              <td>
                <button class="btn btn-info" type="button" name="button">
                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </button>
                <button class="btn btn-danger" type="button" name="button">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
              </td>
            </tr>
          @endforeach
        </table>
      @endif
    </div>
  </div>
@endsection
