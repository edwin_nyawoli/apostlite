@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>Programs and Events</h4>
    </div>
    <div class="panel-body">
        @if(count($programs) == 0)
          No program has been added yet. Click <span style="font-weight: bold;">Add New</span> to add a program.
          <a class="btn btn-success" href="/programs/create">Add New</a>
        @else
          <a class="btn btn-success" href="/programs/create">Add New</a>
          <h4>{{ count($programs) }} Program(s).</h4>
          <table class="table table-hover">
            <tr>
              <th>Name</th>
              <th>Start date <small>(and time)</small></th>
              <th>End date</th>
              <th>Venue</th>
              <th>Actions</th>
            </tr>
            @foreach($programs as $program)
              <tr>
                <td>{{ $program->name }}</td>
                <td>{{ $program->start_date }}</td>
                <td>{{ \Carbon\Carbon::parse($program->end_date)->toDateString() }}</td>
                <td>{{ $program->venue }}</td>
                <td>
                  <!--  TODO Replace id with slug -->
                  <a class="btn btn-info" href="/programs/{{ $program->slug }}/edit">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                  </a>
                  <button class="btn btn-danger btn-del" type="button" name="button" data-toggle="modal" data-target="#confirmDelete" data-index="{{ $program->id }}">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                  </button>
                </td>
              </tr>
            @endforeach
          </table>
        @endif
    </div>
  </div>

  <!-- Confirm delete modal -->
  <div class="modal fade" id="confirmDelete">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <span class="modal-title">Delete Program</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this program?</p>
          <form id="deleteItem" action="{{ url('/programs')}}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" form="deleteItem" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    var form = $('#deleteItem');
    var delBtns = $('.btn-del').click(function(){
      form.attr('action', '/programs/' + $(this).data('index'));
    });
  </script>
@endsection
