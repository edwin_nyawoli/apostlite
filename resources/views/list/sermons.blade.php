@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>Sermons</h4>
    </div>
    <div class="panel-body">
      @if(count($sermons) == 0)
        No sermon has been added yet. Click <span style="font-weight: bold;">Add New</span> to add the sermon from this week.
        <a class="btn btn-success" href="/sermons/create">Add New</a>
      @else
        <a class="btn btn-success" href="/sermons/create">Add New</a>
        <h4>{{ count($sermons) }} Sermon(s).</h4>

        <table class="table table-hover">
          <tr>
            <th>Title</th>
            <th>Minister</th>
            <th>Publish Date</th>
            @if(Auth::user())
              <th>Actions</th>
            @endif
          </tr>
          @foreach($sermons as $sermon)
            <tr>
              <td>{{ $sermon->title }}</td>
              <td>{{ $sermon->minister }}</td>
              <td>{{ $sermon->created_at }}</td>
              @if(Auth::user())
                <td>
                  <!--  TODO Replace id with slug -->
                  <a class="btn btn-info" href="/sermons/{{ $sermon->slug }}/edit">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                  </a>
                  <button type="button" class="btn btn-danger btn-del" data-toggle="modal" data-target="#confirmDelete" data-index="{{ $sermon->id }}">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                  </button>
                </td>
              @endif
            </tr>
          @endforeach
        </table>
      @endif
    </div>
  </div>

  <!-- Confirm delete modal -->
  <div class="modal fade" id="confirmDelete">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <span class="modal-title">Delete Sermon</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this sermon?</p>
          <form id="deleteItem" action="{{ url('/sermons')}}" method="post">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" form="deleteItem" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    var form = $('#deleteItem');
    var delBtns = $('.btn-del').click(function(){
      form.attr('action', '/sermons/' + $(this).data('index'));
    });
  </script>
@endsection
