@extends('layouts.app')

@section('content')
<?php $currentYear = Carbon\Carbon::now()->year ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>Edit Executive</h3>
  </div>
  <div class="panel-body">
    <form method="post" action="{{ url('/wings/executives', $hash)}}">
      <fieldset>
        <legend>Personal Info</legend>
        <div class="form-group">
          <label for="first_name">First Name</label>
          <input class="form-control" type="text" name="first_name" value="{{ $wingExecutive->first_name }}" placeholder="Eg; David" required="required">
        </div>
        <div class="form-group">
          <label for="last_name">Last Name</label>
          <input class="form-control" type="text" name="last_name" value="{{ $wingExecutive->last_name }}" placeholder="Eg; Antwi" required="required">
        </div>
        <div class="form-group">
          <label for="">Bio</label>
          <textarea class="form-control" name="bio" rows="8" cols="80">{{ $wingExecutive->bio }}</textarea>
        </div>
        <div class="form-group">
          <label for="">Profile Photo</label>
          <input class="form-control-file" type="file" name="" value="">
        </div>
      </fieldset>

      <fieldset>
        <legend>Wing Related</legend>
        <div class="form-group">
          <label for="">Wing</label>
          <small>(Only wings that have been previously saved will appear in the list below)</small>
          <select class="form-control" name="wing" required="required">
            @foreach($wings as $wing)
              @if($wing->id == $wingExecutive->wing_id)
                <option value="{{ $wing->name }}" selected> {{ $wing->name }}</option>
              @else
                <option value="{{ $wing->name }}"> {{ $wing->name }}</option>
              @endif
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="position">Position</label>
          <input class="form-control" type="text" name="position" value="{{ $wingExecutive->position }}" placeholder="Eg; President, Treasurer" required="required">
        </div>
        <div class="form-group">
          <label for="">Year Group</label>
          <select class="form-control" name="year_group" required="required">

            @for($i = 0; $i < 10; $i++)
              @if($yearGroups[$i] == $wingExecutive->year_group)
                <option value="{{ $yearGroups[$i] }}" selected>{{ $yearGroups[$i] }}</option>
              @else
                <option value="{{ $yearGroups[$i] }}">{{ $yearGroups[$i] }}</option>
              @endif
            @endfor
          </select>
        </div>
      </fieldset>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Update">
      </div>
    </form>
  </div>
</div>
@endsection
