@extends('layouts.app')

@section('content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>Edit Sermon</h3>
  </div>
  <div class="panel-body">
    <form id="send-form" class="" action="{{ url('/sermons', $hash)}}" method="post">
      <input type="hidden" name="_method" value="patch">
      <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" type="text" name="title" value="{{ $sermon->title }}" placeholder="Title of the sermon" required="required">
      </div>
      <div class="form-group">
        <label for="minister">Minister</label>
        <input class="form-control" type="text" name="minister" value="{{ $sermon->minister }}" placeholder="The minister who delivered this sermon" required="required">
      </div>
      <div class="form-group">
        <label for="scriptures">Scriptures</label>
        <input class="form-control" type="text" name="scriptures" value="{{ $sermon->scriptures}}" placeholder="A list of the scriptures used, separated by commas(,)" required="required">
      </div>
      <div class="form-group">
        <label for="message">Message</label>
        <textarea class="form-control" name="message" rows="8" required="required">{{ $sermon->message }}</textarea>
      </div>
      <div class="form-group">
        <label for="image">Hero Image</label>
        <input type="file" name="image" value="">
      </div>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Update">
      </div>
    </form>
  </div>
</div>
@endsection
