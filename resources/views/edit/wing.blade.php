@extends('layouts.app')

@section('content')
<?php $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday') ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>Edit Wing</h3>
  </div>
  <div class="panel-body">
    <fieldset>
      <form id="send-form" class="" action="{{ url('/wings', $hash)}}" method="post">
        <input type="hidden" name="_method" value="patch">
        <legend>General Information</legend>
        <div class="form-group">
          <label for="wing_name">Wing Name</label>
          <input class="form-control" type="text" name="name" value="{{ $wing->name }}" placeholder="The name of this wing" required="required">
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <input class="form-control" type="text" name="description" value="{{ $wing->description }}" placeholder="A short description of the function of the wing in the church" required="required">
        </div>
        <div class="form-group">
          <label for="slogan">Slogan</label>
          <input class="form-control" type="text" name="slogan" value="{{ $wing->slogan }}" placeholder="..." required="required">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input class="form-control" type="text" name="email" value="{{ $wing->email }}" placeholder="wing@nupsgknust.org" required="required">
        </div>
      </form>
    </fieldset>
    <fieldset>
      <legend>Meeting Times</legend>
      <small>Please provide at least one meeting time.</small>
      @forelse($wingMeetingInfos as $meetingInfo)
      <div id="time-info" class="form-group time-info-set">
        <div class="form-inline">
          <div class="input-group">
            <label for="venue">Location</label>
            <input class="form-control" type="text" name="location[]" value="{{ $meetingInfo->location }}" placeholder="The location where the meeting would be held" required="required">
          </div>
          <div class="input-group">
            <label for="day">Day</label>
            <select class="form-control custom-select" name="day[]" required="required">
              @foreach($days as $day)
                @if($day == $meetingInfo->day)
                  <option value="{{ $day }}" selected>{{ $day }}</option>
                @else
                  <option value="{{ $day }}">{{ $day }}</option>
                @endif
              @endforeach
            </select>
          </div>
          <div class="input-group mb-4">
            <label for="time">Time</label>
            <input class="form-control" type="time" name="time[]" value="{{ $meetingInfo->time }}" required="required">
          </div>
        </div>
      </div>
      @empty
        <div id="time-info" class="form-group time-info-set">
          <div class="form-inline">
            <div class="input-group">
              <label for="venue">Location</label>
              <input class="form-control" type="text" name="location[]" value="" placeholder="The location where the meeting would be held" required="required">
            </div>
            <div class="input-group">
              <label for="day">Day</label>
              <select class="form-control custom-select" name="day[]" required="required">
                <option value="sunday">Sunday</option>
                <option value="monday">Monday</option>
                <option value="tuesday">Tuesday</option>
                <option value="wednesday">Wednesday</option>
                <option value="thursday">Thursday</option>
                <option value="friday">Friday</option>
                <option value="saturday">Saturday</option>
              </select>
            </div>
            <div class="input-group mb-4">
              <label for="time">Time</label>
              <input class="form-control" type="time" name="time[]" value="" required="required">
            </div>
          </div>
        </div>
      @endforelse
      <div class="form-group">
        <button class="btn btn-info" type="button" name="button">Add New Meeting Time</button>
      </div>
    </fieldset>
    <fieldset>

    </fieldset>
    {{ csrf_field() }}
    <div class="form-group">
      <input class="btn btn-success" type="submit" name="" value="Update">
    </div>
  </div>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var timeInfoClone = $('#time-info').clone(false);
    $('#add-new-time').click(function(){
      timeInfoClone.clone(false).insertAfter('div.time-info-set:last');
    });
  </script>
@endsection
