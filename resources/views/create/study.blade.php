@extends('layouts.app')

@section('content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>New Study</h3>
  </div>
  <div class="panel-body">
    <form id="send-form" class="" action="/studies" method="post">
      <div class="form-group">
        <label for="study_number">Study Number </label>
        <input class="form-control" type="number" name="study_number" value="" placeholder="The Study number: 1, 2, 3, ..." required="required">
      </div>
      <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" type="text" name="title" value="" placeholder="The title of the study" required="required">
      </div>
      <div class="form-group">
        <label for="scriptures">Scriptures</label>
        <input class="form-control" type="text" name="scriptures" value="" placeholder="A list of scriptures separated by a comma(,)" required="required">
      </div>
      <div class="form-group">
        <label for="content">Body</label>
        <textarea class="form-control" name="content" rows="8" required="required"></textarea>
      </div>
      <div class="form-group">
        <label for="questions">Questions</label>
        <textarea class="form-control" name="questions" rows="8"></textarea>
      </div>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Save">
      </div>
    </form>
  </div>
</div>
@endsection
