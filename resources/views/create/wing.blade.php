@extends('layouts.app')

@section('content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>New Wing</h3>
  </div>
  <div class="panel-body">
    <form id="send-form" class="" action="{{ url('/wings') }}" method="post">
      <fieldset>
        <legend>General Information</legend>
          <div class="form-group">
            <label for="wing_name">Wing Name</label>
            <input class="form-control" type="text" name="name" value="" placeholder="The name of this wing" required="required">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <input class="form-control" type="text" name="description" value="" placeholder="A short description of the function of the wing in the church" required="required">
          </div>
          <div class="form-group">
            <label for="slogan">Slogan</label>
            <input class="form-control" type="text" name="slogan" value="" placeholder="..." required="required">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="text" name="email" value="" placeholder="wing@nupsgknust.org" required="required">
          </div>
        </fieldset>
      <fieldset>
        <legend>Meeting Times</legend>
        <small>Please provide at least one meeting time.</small>
        <div id="time-info" class="form-group time-info-set">
          <div class="form-inline">
            <div class="input-group">
              <label for="venue">Location</label>
              <input class="form-control" type="text" name="location[]" value="" placeholder="The location where the meeting would be held" required="required">
            </div>
            <div class="input-group">
              <label for="day">Day</label>
              <select class="form-control custom-select" name="day[]" required="required">
                <option value="sunday">Sunday</option>
                <option value="monday">Monday</option>
                <option value="tuesday">Tuesday</option>
                <option value="wednesday">Wednesday</option>
                <option value="thursday">Thursday</option>
                <option value="friday">Friday</option>
                <option value="saturday">Saturday</option>
              </select>
            </div>
            <div class="input-group mb-4">
              <label for="time">Time</label>
              <input class="form-control" type="time" name="time[]" value="" required="required">
            </div>
          </div>
        </div>

        <div class="form-group">
          <button id="add-new-time" class="btn btn-info" type="button" name="button">Add New Meeting Time</button>
        </div>
      </fieldset>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Save">
      </div>
    </form>
  </div>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var timeInfoClone = $('#time-info').clone(false);
    $('#add-new-time').click(function(){
      timeInfoClone.clone(false).insertAfter('div.time-info-set:last');
    });
  </script>
@endsection
