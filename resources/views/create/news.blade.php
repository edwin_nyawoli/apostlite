@extends('layouts.app')

@section('content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>New News Post</h3>
  </div>
  <div class="panel-body">
    <form id="send-form" class="" action="/news" method="post">
      <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" type="text" name="title" value="" required="required" placeholder="The title of this news / notification">
      </div>
      <div class="form-group">
        <label for="content">Body</label>
        <textarea class="form-control" rows="8" name="content" required="required"></textarea>
      </div>
      {{ csrf_field() }}
      <div class="form-group">
        <button type="submit" class="btn btn-success">Save</button>
        <button id="form-submit-btn" type="submit" class="btn btn-primary">Save and Send</button>
      </div>
    </form>
  </div>
</div>

@endsection
