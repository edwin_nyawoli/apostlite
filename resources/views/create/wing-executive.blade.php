@extends('layouts.app')

@section('content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>New Executive</h3>
  </div>
  <div class="panel-body">
    <form method="post" action="/wings/executives">
      <fieldset>
        <legend>Personal Info</legend>
        <div class="form-group">
          <label for="first_name">First Name</label>
          <input class="form-control" type="text" name="first_name" value="" placeholder="Eg; David" required="required">
        </div>
        <div class="form-group">
          <label for="last_name">Last Name</label>
          <input class="form-control" type="text" name="last_name" value="" placeholder="Eg; Antwi" required="required">
        </div>
        <div class="form-group">
          <label for="">Bio</label>
          <textarea class="form-control" name="bio" rows="8" cols="80" placeholder="..."></textarea>
        </div>
        <div class="form-group">
          <label for="">Profile Photo</label>
          <input class="form-control-file" type="file" name="" value="">
        </div>
      </fieldset>

      <fieldset>
        <legend>Wing Related</legend>
        <div class="form-group">
          <label for="">Wing</label>
          <small>(Only wings that have been previously saved will appear in the list below)</small>
          <select class="form-control" name="wing_name" required="required">
            @foreach($wingNames as $wingName)
              <option value="{{ $wingName->name }}"> {{ $wingName->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="position">Position</label>
          <input class="form-control" type="text" name="position" value="" placeholder="Eg; President, Treasurer" required="required">
        </div>
        <div class="form-group">
          <label for="">Year Group</label>
          <select class="form-control" name="year_group" required="required">
            @foreach($yearGroups as $yearGroup)
                <option value="{{ $yearGroup }}">{{ $yearGroup }}</option>
            @endforeach
          </select>
        </div>
      </fieldset>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Save">
      </div>
    </form>
  </div>
</div>
@endsection
