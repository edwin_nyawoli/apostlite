@extends('layouts.app')

@section('content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>New Program / Event </h3>
  </div>
  <div class="panel-body">
    <form id="send-form" class="" action="/programs" method="post">
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" name="name" value="" placeholder="The name of the program / event" required="required">
      </div>
      <div class="form-group">
        <label for="start_date">Start date <small>(and time)</small></label>
        <input class="form-control" type="date" name="start_date" value="{{ date('Y-m-d') }}" required="required">
      </div>
      <div class="form-group">
        <label for="end_date">End date</label>
        <input class="form-control" type="date" name="end_date" value="" required="required">
      </div>
      <div class="form-group">
        <label for="venue">Venue</label>
        <input class="form-control" type="text" name="venue" value="" placeholder="Location of this event" required="required">
      </div>
      <div class="form-group">
        <label for="banner">Banner Image</label>
        <input class="form-control-file" type="file" name="banner" value="">
      </div>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Save">
      </div>
    </form>
  </div>
</div>
@endsection
