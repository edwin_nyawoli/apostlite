@extends('layouts.app')

@section('content')
<?php $currentYear = Carbon\Carbon::now()->year ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3>New Wing Image</h3>
  </div>
  <div class="panel-body">
    <form id="send-form" class="" action="/wings/gallery" method="post">
      <div class="form-group">
        <label for="">Wing</label>
        <small>(Only wings that have been previously saved will appear in the list below)</small>
        <select class="form-control" name="wing_name" required="required">
          @foreach($wingNames as $wingName)
            <option value="{{ $wingName->name }}"> {{ $wingName->name }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Year Group</label>
        <select class="form-control" name="year_group" required="required">
          @for ($i = 0; $i < 10; $i++)
              <option value="{{ ($currentYear - $i) . '-' . ($currentYear - $i + 1) }}">{{ ($currentYear - $i) . '-' . ($currentYear - $i + 1) }}</option>
          @endfor
        </select>
      </div>
      <div class="form-group">
        <label for="">Image</label>
        <input class="form-control-file" type="file" name="group_image" value="" required="required">
      </div>
      {{ csrf_field() }}
      <div class="form-group">
        <input class="btn btn-success" type="submit" name="" value="Save">
      </div>
    </form>
  </div>
</div>
@endsection
