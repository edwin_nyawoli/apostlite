@extends('layouts.app')

@section('content')
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>
        Dashboard
      </h4>
    </div>
    <div class="panel-body">
      Welcome {{ Auth::user()->name }}.
      <p>
        The NUPSG android application on the devices of user's would be notified
        of any changes to data made here.
      </p>
    </div>
  </div>
@endsection
